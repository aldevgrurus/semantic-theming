import './App.css';
import React from "react";
import ThemingLayout from "./ThemingLayout";

function App() {
  return (
    <div className="App">
      <ThemingLayout/>
    </div>
  );
}

export default App;
